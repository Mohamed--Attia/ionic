import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AddContactPage } from '../pages/addContact/addContact';
import { ContactPage } from '../pages/contact/contact';
import { ContactListPage } from '../pages/contactList/contactList';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { RegisterPage } from '../pages/register/register';
import {editContactPage} from '../pages/editContact/editContact'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {UserProfilePage} from '../pages/user-profile//user-profile'
//step1
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { IonicStorageModule } from '@ionic/storage';

//step2
const config = {
  apiKey: "AIzaSyBIwALDtJzUBM_M-J2H4De0P6NiNYoN6H4",
  authDomain: "ionicdb-1b9d9.firebaseapp.com",
  databaseURL: "https://ionicdb-1b9d9.firebaseio.com",
  projectId: "ionicdb-1b9d9",
  storageBucket: "ionicdb-1b9d9.appspot.com",
  messagingSenderId: "926156584375"
};

@NgModule({
  declarations: [
    MyApp,
    AddContactPage,
    ContactPage,
    ContactListPage,
    TabsPage,
    editContactPage,
    LoginPage,
    RegisterPage,
    UserProfilePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    //step3
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddContactPage,
    ContactPage,
    ContactListPage,
    TabsPage,
    editContactPage,
    LoginPage,
    RegisterPage,
    UserProfilePage
  ],
  providers: [
    StatusBar,
    //4
    AngularFireDatabase,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
