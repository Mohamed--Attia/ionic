import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ContactListPage } from '../contactList/contactList';
import { NgModule } from '@angular/core';
import { ContatcService } from '../shared/contacts'
import { AlertController  } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { ContactPage } from '../contact/contact'
import { AngularFireAuth } from 'angularfire2/auth';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-about',
  templateUrl: 'addContact.html',
  providers: [ContatcService]
})

export class AddContactPage {

  constructor(private auth:AngularFireAuth,
    private alertCtrl: AlertController ,
    private contatcService: ContatcService,
    private db: AngularFireDatabase,
    public navCtrl: NavController) { }

    ngOnInit() {
      this.auth.auth.onAuthStateChanged(user => {
        if (!user) {
          this.navCtrl.setRoot(ContactPage)
        }
      })
    }

  onSaveFormData(name, lname, department, age, information) {
    if (name != null && lname != null && department != null && age != null && information != null) {
      this.contatcService.addContact(name, lname, department, age, information);
      this.navCtrl.setRoot(ContactListPage);
      
    } else {
      let alert = this.alertCtrl.create({
        title: 'Missed Data',
        subTitle: 'Please Make Sure That You Entered All Data!',
        buttons: ['OK']
      });
      alert.present();
    }
  }

}