import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { ContactListPage } from '../contactList/contactList';
import { RegisterPage } from '../register/register';
import { UserProfilePage } from '../user-profile/user-profile'
import firebase from 'firebase'
import { Storage } from '@ionic/storage';
@IonicPage()

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  email = '';
  password = '';
  constructor(private storage: Storage,
    public fire: AngularFireAuth,
    public navCtrl: NavController,
    public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  login() {
    this.fire.auth.signInWithEmailAndPassword(this.email, this.password).then(user => {
      this.navCtrl.push(ContactListPage);
    }, function (error) {
      console.log("error", error)
    });
  }

  goRegisterPage() {
    this.navCtrl.push(RegisterPage)
  }

  loginByGoogle() {
    this.fire.auth.signInWithPopup(new
      firebase.auth.GoogleAuthProvider()).then(res => {
        if (res) {
          this.storage.set('profileData', {
            name: res.additionalUserInfo.profile.name,
            img: res.additionalUserInfo.profile.picture,
            gender: res.additionalUserInfo.profile.gender,
            email: res.additionalUserInfo.profile.email
          });
          this.navCtrl.push(UserProfilePage, {
            name: res.additionalUserInfo.profile.name,
            img: res.additionalUserInfo.profile.picture,
            gender: res.additionalUserInfo.profile.gender,
            email: res.additionalUserInfo.profile.email
          })
        } else {
          console.log("error")
        }
      })
  }

}
