import { Injectable } from '@angular/core';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';


@Injectable() 
export class ContatcService {
    contactList: AngularFireList<any>;
    constructor(private angularfireDb: AngularFireDatabase) { }

    getContactList() {
       this.contactList = this.angularfireDb.list('people');
        return this.contactList;
    }

    addContact(name, lname, department, age, information) {
        this.contactList = this.angularfireDb.list('people');
        this.contactList.push({
            name: name,
            lname: lname,
            department: department,
            information: information,
            age: age
        });
    }//end addContact

    removeContact($key: string) {
        this.contactList.remove($key);
    }

}
