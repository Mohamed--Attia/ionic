import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';
import { ContatcService } from '../shared/contacts'
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { editContactPage } from '../editContact/editContact';
import { LoginPage } from '../login/login';
import {ContactPage} from '../contact/contact'
@Component({
  selector: 'page-home',
  templateUrl: 'contactList.html',
  providers: [ContatcService]
})

export class ContactListPage {
  contactList: any[];

  constructor(private fauth: AngularFireAuth,
    private contatcService: ContatcService,
    private db: AngularFireDatabase,
    public navCtrl: NavController) { }
    
    ionViewDidLoad() {
      this.checkUser();
    }

  ngOnInit() {
  }

  checkUser(){
    this.fauth.auth.onAuthStateChanged(user => {
      if (user) {
        console.log("user",user)
        this.getPeopleList();
      }
      else{
        this.navCtrl.setRoot(ContactPage)
      }
    })
  }

  getPeopleList() {
    this.contatcService.getContactList().snapshotChanges()
      .subscribe(res => {
        this.contactList = [];
        res.forEach(element => {
          var x = element.payload.toJSON();
          x["$key"] = element.key;
          this.contactList.push(x);
        })
      });
  }//end retriveData

  deleteContact($key) {
    this.contatcService.removeContact($key)
  }

  updateContact($key, name, lname, department, age, information) {
    this.navCtrl.push(editContactPage, {
      key: $key,
      name: name,
      lname: lname,
      department: department,
      age: age,
      information: information
    })
  }

}
