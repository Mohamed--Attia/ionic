import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { AngularFireAuth } from 'angularfire2/auth';
import { ContactPage } from '../contact/contact'

/**
 * Generated class for the UserProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
  userInfo = {
    name: '',
    img: '',
    email: '',
    loggin: false,
    gender:''
  }
  constructor(private auth:AngularFireAuth,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserProfilePage');
  }
  ngOnInit() {
    this.auth.auth.onAuthStateChanged(user => {
      if (!user) {
        this.navCtrl.setRoot(ContactPage)
      }
    })

    this.storage.get('profileData').then((userData) => {
      if(userData){
      this.userInfo.email = userData.email;
      this.userInfo.img = userData.img;
      this.userInfo.name = userData.name;
      this.userInfo.gender = userData.gender;
    }
    });
  }
}
