import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegisterPage } from '../register/register';
import { AngularFireAuth } from 'angularfire2/auth';
import { ContactListPage } from '../contactList/contactList';
import firebase from 'firebase'

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

authFlag:boolean = false;

constructor(public fAuth: AngularFireAuth,
    public navCtrl: NavController) { }

  ngOnInit() {
    this.checkAuth();
  }

  checkAuth(){
    this.fAuth.auth.onAuthStateChanged(user => {
      if (user) {
       this.authFlag = true;
      }
    })
  }
  
  logIn() {
    this.navCtrl.push(LoginPage);
  }

  registerByEmail() {
    this.navCtrl.push(RegisterPage);
  }

  logOut() { 
    this.authFlag = false;
    this.fAuth.auth.signOut();
  }
  
}
