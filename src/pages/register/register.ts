import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController, ToastController } from 'ionic-angular';
import { ContactListPage } from '../contactList/contactList'
import {LoginPage} from '../login/login' 
/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  constructor(public alertCtrl: AlertController,
    private toastCtrl: ToastController,
    public fire: AngularFireAuth,
    public navCtrl: NavController,
    public navParams: NavParams) {
  }
  email: string = '';
  password: string = '';
  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  register() {
    this.fire.auth.createUserWithEmailAndPassword(this.email, this.password).then(user => {
      this.navCtrl.push(ContactListPage)
    }).catch(function (error) {
      // Handle Errors here.
      let errorCode = error.code;
      let errorMessage = error.message;
    });
  }


}
