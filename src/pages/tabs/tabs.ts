import { Component } from '@angular/core';

import { AddContactPage } from '../addContact/addContact';
import { ContactPage } from '../contact/contact';
import { ContactListPage } from '../contactList/contactList';
import {UserProfilePage} from '../user-profile/user-profile'

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = ContactListPage;
  tab2Root = AddContactPage;
  tab3Root = ContactPage;
  tab4Root = UserProfilePage

  constructor() {

  }
  
}
