import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ContactListPage } from '../contactList/contactList';
import { NgModule } from '@angular/core';
import { ContatcService } from '../shared/contacts'

import { AngularFireDatabase, AngularFireList, AngularFireObject } from 'angularfire2/database';

import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'page-edit',
  templateUrl: 'editContact.html',
  providers: [ContatcService]
})

export class editContactPage {
  contactList: AngularFireList<any>;
  contact = {
    key: '',
    name: '',
    lname: '',
    department: '',
    age: '',
    information: ''
  };
  constructor(private contatcService: ContatcService,
    private angularfireDb: AngularFireDatabase,
    private navParams: NavParams,
    public navCtrl: NavController) { }

  ngOnInit() {
    this.contactList = this.angularfireDb.list('people');
    this.contact.key = this.navParams.get('key');
    this.contact.name = this.navParams.get('name');
    this.contact.lname = this.navParams.get('lname');
    this.contact.department = this.navParams.get('department');
    this.contact.age = this.navParams.get('age');
    this.contact.information = this.navParams.get('information');
  }

  onUpdateContactData() {
    this.contactList.update(this.contact.key, {
      name: this.contact.name,
      lname: this.contact.lname,
      department: this.contact.department,
      age: this.contact.age,
      information: this.contact.information
    }).then(res => {
      this.navCtrl.setRoot(ContactListPage);
    })
  }

}